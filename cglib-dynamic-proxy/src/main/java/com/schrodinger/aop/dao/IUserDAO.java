package com.schrodinger.aop.dao;

import com.schrodinger.aop.model.User;

/**
 * Created by zhangli on 2017/9/19.
 */
public interface IUserDAO {
    void addUser(User user);
}
