package com.schrodinger.aop;

import com.schrodinger.aop.dao.IUserDAO;
import com.schrodinger.aop.dao.UserDAO;
import com.schrodinger.aop.model.User;
import com.schrodinger.aop.proxy.CGLIBProxy;

/**
 * Created by zhangli on 2017/9/19.
 */
public class Client {
    public static void main(String[] args) {
        CGLIBProxy proxy = new CGLIBProxy();
        //生成子类，创建代理类
        IUserDAO userDAO = (UserDAO)proxy.getProxy(UserDAO.class);
        userDAO.addUser(new User());
    }
}
