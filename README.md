# spring-aop-samples
示例包含简单的静态代理，jdk动态代理，cglib动态代理，spring编程式AOP, spring基于xml的声明式AOP, spring基于aspectj的@Aspect注解的声明式AOP, spring基于aspectj的xml注解的声明式AOP

## spring-aop-declaratively-xml
虽然这种方式没有启用@Aspect注解，但是用到Aspectj style pointcut expression, 所以也要引aspectjweaver.jar

##spring1.2 spring AOP配置与最新spring AOP配置
spring1.2方式：  
spring-aop-programmatically-advisor     
spring-aop-declaratively-xml   
最新方式：   
spring-aop-programmatically-AspectJProxyFactory      
spring-aop-declaratively-aspectj-annotation   
spring-aop-declaratively-aspectj-xml