package com.schrodinger.aop;

import com.schrodinger.aop.dao.IUserDAO;
import com.schrodinger.aop.dao.UserDAO;
import com.schrodinger.aop.model.User;
import com.schrodinger.aop.proxy.UserDAOProxy;

/**
 * Created by zhangli on 2017/9/19.
 */
public class Client {
    public static void main(String[] args) {
        IUserDAO userDAO = new UserDAOProxy(new UserDAO());
        userDAO.addUser(new User());
    }
}
