package com.schrodinger.aop.proxy;

import com.schrodinger.aop.dao.IUserDAO;
import com.schrodinger.aop.model.User;

/**
 * Created by zhangli on 2017/9/19.
 */
public class UserDAOProxy implements IUserDAO {

    private IUserDAO userDAO;

    public UserDAOProxy(IUserDAO userDAO){
        this.userDAO = userDAO;
    }

    public void addUser(User user) {
        try {
            System.out.println("begin addUser...");
            userDAO.addUser(user);
            System.out.println("end addUser...");
        } catch (Exception e) {
            System.out.println("addUser error!");
            e.printStackTrace();
        }
    }
}
