package com.schrodinger.aop;

import com.schrodinger.aop.proxy.LoggerAfterReturningAdvice;
import com.schrodinger.aop.proxy.LoggerMethodBeforeAdvice;
import com.schrodinger.aop.proxy.SecurityMethodInterceptor;
import com.schrodinger.aop.service.IUserService;
import com.schrodinger.aop.service.UserService;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.support.DefaultPointcutAdvisor;

/**
 * Created by zhangli on 2017/9/19.
 */
public class ProxyFactoryBeanClient {
    public static void main(String[] args) {
        //1.初始化源对象(一定要实现接口)
        IUserService target = new UserService();
        //2.AOP 代理工厂
        ProxyFactoryBean pf = new ProxyFactoryBean();
        pf.setTarget(target);
        //3.装配Advice
        pf.addAdvice(new SecurityMethodInterceptor());
//        pf.addAdvice(new LoggerMethodBeforeAdvice());
        pf.addAdvisor(new DefaultPointcutAdvisor(new LoggerMethodBeforeAdvice()));
        pf.addAdvisor(new DefaultPointcutAdvisor(new LoggerAfterReturningAdvice()));
        ////4.获取代理对象
        IUserService proxy = (IUserService) pf.getObject();
        IUserService proxy2 = (IUserService) pf.getObject();
        //5.调用业务
        proxy.addUser();
    }
}
