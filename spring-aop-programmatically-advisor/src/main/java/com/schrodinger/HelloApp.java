package com.schrodinger;

import com.schrodinger.config.HelloConfig;
import com.schrodinger.service.IHelloService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by zhangli on 2017/9/22.
 */
public class HelloApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(HelloConfig.class);
        IHelloService helloService = ac.getBean(IHelloService.class);
        IHelloService helloService2 = ac.getBean(IHelloService.class);
        helloService.sayHello();

    }
}
