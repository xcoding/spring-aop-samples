package com.schrodinger.service;

/**
 * Created by zhangli on 2017/9/22.
 */
public interface IHelloService {
    void sayHello();
}
