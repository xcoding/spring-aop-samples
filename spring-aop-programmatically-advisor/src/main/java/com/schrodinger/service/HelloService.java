package com.schrodinger.service;

/**
 * Created by zhangli on 2017/9/22.
 */
public class HelloService implements IHelloService {
    @Override
    public void sayHello() {
        System.out.println("hello world!");
    }
}
