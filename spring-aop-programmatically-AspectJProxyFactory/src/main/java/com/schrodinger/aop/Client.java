package com.schrodinger.aop;

import com.schrodinger.aop.proxy.LoggerAspect;
import com.schrodinger.aop.service.IUserService;
import com.schrodinger.aop.service.UserService;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;

/**
 * Created by zhangli on 2017/9/19.
 */
public class Client {
    public static void main(String[] args) {
        IUserService target = new UserService();
        AspectJProxyFactory factory = new AspectJProxyFactory(target);
        factory.addAspect(LoggerAspect.class);
        IUserService proxy = factory.getProxy();
        IUserService proxy2 = factory.getProxy();
        proxy.addUser();
    }
}
