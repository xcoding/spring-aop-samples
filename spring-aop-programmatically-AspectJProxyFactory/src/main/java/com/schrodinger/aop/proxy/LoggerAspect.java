package com.schrodinger.aop.proxy;

/**
 * Created by zhangli on 2017/9/19.
 */
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

@Aspect
public class LoggerAspect {
    //前置增强
    @Before(value="execution(* com.schrodinger.aop.service..*.*(..))")
    public void asBefore(){
        System.out.println("@Before");
    }
    //后置增强
    @AfterReturning(value="execution(* com.schrodinger.aop.service..*.*(..))")
    public void asAfterReturning(){
        System.out.println("@AfterReturning");
    }
    //环绕增强
    @Around(value="execution(* com.schrodinger.aop.service..*.*(..))")
    public void asAround(ProceedingJoinPoint pj){
        System.out.println("@Around enter");
        try {
            pj.proceed();
        } catch (Throwable e) {
            //抓捕异常
            e.printStackTrace();
        }
        System.out.println("@Around out");
    }
    //异常增强
    @AfterThrowing(value="execution(* com.schrodinger.aop.service..*.*(..))")
    public void asThorws(){
        System.out.println("@AfterThrowing");
    }
    //最终增强
    @After(value="execution(* com.schrodinger.aop.service..*.*(..))")
    public void asAfter(){
        System.out.println("@After");
    }
}
