package com.schrodinger.aop.model;

/**
 * Created by zhangli on 2017/9/19.
 */
public class User {

    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
