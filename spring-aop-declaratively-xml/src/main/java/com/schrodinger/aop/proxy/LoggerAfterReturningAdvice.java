package com.schrodinger.aop.proxy;

import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * Created by zhangli on 2017/9/19.
 */
public class LoggerAfterReturningAdvice implements AfterReturningAdvice {

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println("=======LoggerAfterReturningAdvice=========");
    }
}
