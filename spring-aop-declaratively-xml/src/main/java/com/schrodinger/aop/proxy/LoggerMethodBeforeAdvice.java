package com.schrodinger.aop.proxy;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * Created by zhangli on 2017/9/19.
 */
public class LoggerMethodBeforeAdvice implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("=======LoggerMethodBeforeAdvice=========");
    }
}
