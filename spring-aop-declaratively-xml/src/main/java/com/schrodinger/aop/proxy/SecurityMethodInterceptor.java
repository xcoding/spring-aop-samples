package com.schrodinger.aop.proxy;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * Created by zhangli on 2017/9/19.
 */
public class SecurityMethodInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        System.out.println("==========执行安全校验前====================");
        Object result = methodInvocation.proceed();
        System.out.println("==========执行安全校验后====================");
        return result;
    }
}
