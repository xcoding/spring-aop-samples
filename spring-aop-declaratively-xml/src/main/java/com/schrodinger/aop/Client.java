package com.schrodinger.aop;

import com.schrodinger.aop.dao.UserDAO;
import com.schrodinger.aop.model.User;
import com.schrodinger.aop.proxy.LoggerAfterReturningAdvice;
import com.schrodinger.aop.proxy.LoggerMethodBeforeAdvice;
import com.schrodinger.aop.proxy.SecurityMethodInterceptor;
import com.schrodinger.aop.service.IUserService;
import com.schrodinger.aop.service.UserService;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by zhangli on 2017/9/19.
 */
public class Client {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("application.xml");

        IUserService proxy = ac.getBean(IUserService.class);
        proxy.addUser(new User());
    }
}
