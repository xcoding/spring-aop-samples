package com.schrodinger.aop.proxy;

/**
 * Created by zhangli on 2017/9/19.
 */

import org.aspectj.lang.ProceedingJoinPoint;

public class LoggerAspect {
    //前置增强
    public void asBefore() {
        System.out.println("@Before");
    }

    //后置增强
    public void asAfterReturning() {
        System.out.println("@AfterReturning");
    }

    //环绕增强
    public void asAround(ProceedingJoinPoint pj) {
        System.out.println("@Around enter");
        try {
            pj.proceed();
        } catch (Throwable e) {
            //抓捕异常
            e.printStackTrace();
        }
        System.out.println("@Around out");
    }

    //异常增强
    public void asThorws() {
        System.out.println("@AfterThrowing");
    }

    //最终增强
    public void asAfter() {
        System.out.println("@After");
    }
}
