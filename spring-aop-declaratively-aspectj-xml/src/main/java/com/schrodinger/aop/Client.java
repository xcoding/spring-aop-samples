package com.schrodinger.aop;

import com.schrodinger.aop.model.User;
import com.schrodinger.aop.service.IUserService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by zhangli on 2017/9/19.
 */
public class Client {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("application.xml");

        IUserService proxy = ac.getBean(IUserService.class);
        proxy.addUser(new User());
    }
}
