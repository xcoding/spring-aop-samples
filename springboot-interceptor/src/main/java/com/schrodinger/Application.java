package com.schrodinger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springMVC:HandlerInterceptor拦截器的使用
 * https://blog.csdn.net/sunp823/article/details/51694662
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
