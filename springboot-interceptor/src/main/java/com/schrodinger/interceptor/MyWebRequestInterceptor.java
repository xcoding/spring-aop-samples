package com.schrodinger.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

public class MyWebRequestInterceptor implements WebRequestInterceptor {

    private static final Logger log = LoggerFactory.getLogger(MyWebRequestInterceptor.class);

    @Override
    public void preHandle(WebRequest request) throws Exception {
        log.info("MyWebRequestInterceptor preHandle");
    }

    @Override
    public void postHandle(WebRequest request, ModelMap model) throws Exception {
        log.info("MyWebRequestInterceptor postHandle");
    }

    @Override
    public void afterCompletion(WebRequest request, Exception ex) throws Exception {
        log.info("MyWebRequestInterceptor afterCompletion");
    }
}
