package com.schrodinger.config;

import com.schrodinger.interceptor.MyHandlerInterceptor;
import com.schrodinger.interceptor.MyWebRequestInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebAppConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册自定义拦截器，添加拦截路径和排除拦截路径
        registry.addWebRequestInterceptor(new MyWebRequestInterceptor()).addPathPatterns("/**").excludePathPatterns("/login");
        registry.addInterceptor(new MyHandlerInterceptor()).addPathPatterns("/**").excludePathPatterns("/login");
    }

}
