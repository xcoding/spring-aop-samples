package com.schrodinger.aop.service;

import com.schrodinger.aop.dao.IUserDAO;
import com.schrodinger.aop.model.User;

/**
 * Created by zhangli on 2017/9/19.
 */
public class UserService implements IUserService {

    private IUserDAO userDAO;

    UserService(IUserDAO userDAO){
        this.userDAO = userDAO;
    }

    public void addUser(User user) {
        userDAO.addUser(user);
    }
}
