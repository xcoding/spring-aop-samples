package com.schrodinger.aop.service;

import com.schrodinger.aop.model.User;

/**
 * Created by zhangli on 2017/9/19.
 */
public interface IUserService {
    void addUser(User user);
}
