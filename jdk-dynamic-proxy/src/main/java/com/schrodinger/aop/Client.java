package com.schrodinger.aop;

import com.schrodinger.aop.dao.IUserDAO;
import com.schrodinger.aop.dao.UserDAO;
import com.schrodinger.aop.model.User;
import com.schrodinger.aop.proxy.LogHandler;

/**
 * Created by zhangli on 2017/9/19.
 */
public class Client {
    public static void main(String[] args) {
        LogHandler logHandler = new LogHandler();
        IUserDAO userDAO = (IUserDAO) logHandler.newProxyInstance(new UserDAO());
        userDAO.addUser(new User());
    }
}
